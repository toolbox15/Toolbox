import { Component } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PhysicalConfigService } from './services/physical-config.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'jarvis';
  configdata:any;
  accessKey=""
  environment=""
  tenantId=""
  url=""
  closeResult:any;
  constructor(private configoutput:PhysicalConfigService, private modalService: NgbModal){
  }
  //show button to show the data
  getValue(val1:string, val2:string, val3:string){
    this.accessKey=val1
    this.environment=val2
    this.tenantId=val3
    this.url="https://"+this.environment+".reltio.com/reltio/tenants/"+this.tenantId+"/"
//    this.url="https://gorest.co.in/"+this.environment+this.tenantId
    console.log(this.accessKey, this.environment, this.tenantId, this.url)
    this.configoutput.getconfigdata(this.url,this.accessKey).subscribe((data)=>{
    this.configdata=data;
    })
  }
  //submit button to set the data
  setValue(val1:string, val2:string, val3:string){
    this.accessKey=val1
    this.environment=val2
    this.tenantId=val3
    this.url="https://"+this.environment+".reltio.com/reltio/tenants/"+this.tenantId+"/"
//    this.url="https://gorest.co.in/"+this.environment+this.tenantId
    console.log(this.accessKey, this.environment, this.tenantId, this.url)
    this.configoutput.setconfigdata(this.url).subscribe((data)=>{
    this.configdata=data;
    })
  }
  
  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
